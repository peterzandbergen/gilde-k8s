#!/bin/bash

# Start contaier met port mapping
docker run -it --rm --name nginx-port -p 8080:80 nginx:alpine

# Go inside container
docker exec -it nginx-port  bash
# inside
cd /usr/share/nginx/html
 
cat <<EOF > index.html
Hello CBS Software Gilde
EOF

