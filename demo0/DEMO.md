# Preparation

## Create cluster

Create cluster, goto the ./kind directory and run 

```
./create-cluster-ingress-nginx.sh
```

This script uses the config file ```cluster-ingress-config.yaml``` to setup a three node cluster and allow traffic to ingress.

```
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true"
  extraPortMappings:
  - containerPort: 80
    hostPort: 80
    protocol: TCP
  - containerPort: 443
    hostPort: 443
    protocol: TCP
- role: worker
- role: worker
```

The the script creates a kind cluster with one control-plan and 3 worker nodes an Nginx ingress controller.

Check the cluster with 
```
watch -n 1 kubectl get nodes
```

## Clean up

Delete the cluster after use:

```
kind delete cluster
```

[TOC](../README.md)