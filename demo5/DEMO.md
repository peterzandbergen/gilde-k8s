# Demo 5: Configure Container with ConfigMap

A well designed container allows its user to change the configuration
using environment variables. The image **peterzandbergen/myipaddress** 
takes its Instancename from the NAME environment variable. If this 
variable is set to RANDOM it generates a random name.

We set the environment variables using a ConfigMap resource. We also
use Kustomize to generate the ConfigMap from the **config.env** file.
This frees us from changing the yaml files for the Deployment.

## ConfigMap

The kustomization.yaml file specifies the configmap **instance-name** with a generator:

```
configMapGenerator:
  - name: instance-name
    envs:
      - config.env
```

The content for the config map are defined in config.env:

```
NAME=RANDOM
```

We use the configmap **instance-name** in the deployment to set 
the environment of the container:

```
spec:
  template:
    spec:
      containers:        
        - envFrom:
          - configMapRef:
              name: instance-name
```

## Demo

Start tmux

```
tmux new-session \; split-window -v \; split-window -h
```

Monitor the application
```
watch -n 1 curl -s http://demo5.cbs-gilde.local
```

Monitor the pods
```
watch -n kubectl get pods -o wide
```

Update the application:
- change NAME in config.env
- apply the changes

```
kustomize build . | kubectl apply -f -
```

[TOC](../README.md)