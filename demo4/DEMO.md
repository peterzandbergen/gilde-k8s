# Demo 4 Deployment

Deployments manage pods. The pods are created using a template. 
The **replicas** defines the number pods the Deployment will try
to have running at all times. If a pod dies, then the Deployment will create
a replacement.

To monitor what's happening in the cluster we need three terminals.

Start tmux with three panes.

```
tmux new-session \; split-window -v \; split-window -h
```


## Start the service

This command scans the directory for *.yaml files and creates the resource from
the definitions.

```
kubectl apply -f .
```

Test de applicatie [http://demo4.cbs-gilde.local](http://demo4.cbs-gilde.local)


## Test what happens when we kill pods

Start a watch with curl on the service.

```
watch -n 1 curl http://demo4.cbs-gilde.local
```

```
watch -n kubectl get pods -o wide
```

Kill a pod

```
kubectl delete pod <podname>
```

With one replica, the service is unavailable for a short period of time. 
This is better than the behavior with a single pod, but not satisfactory.

Scale up the number of pods in the **nginx-deployment.yaml** and apply the scripts.

See what happens if you kill a pod.

Cleanup

```
kubectl delete -f .
```


[TOC](../README.md)