# Demo 2: Single POD

## Imperative

Start pod met Alpine Linux met terminal

```
kubectl run alpine-pod-1 -it --image=alpine 
```

Attach to running pod met terminal

```
kubectl attach alpine-pod-1 -it
```

Clean up

```
kubectl delete alpine-pod-1
```

## Declarative

Create pod from yaml file. The pod needs to stay alive.

```
kubctl apply -f alpine-pod.yaml
```

Connect to the running pod, run shell

```
kubectl exec --stdin --tty alpine-2 -- sh
```

Clean up

```
kubectl delete -f alpine-pod.yaml
```


[TOC](../README.md)