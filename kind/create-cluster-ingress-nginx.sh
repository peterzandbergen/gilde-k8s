#!/bin/bash

kind create cluster --config=cluster-ingress-config.yaml

echo
echo Installing Nginx ingress controller
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/kind/deploy.yaml
