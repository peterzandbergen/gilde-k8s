# Demo 3: Make pods accessible

A pod cannot be accessed from outside the pod. This requires a Service. A Service provides a stable endpoint for the Pods that can come and go. A Service directs the received traffic to the pods based on a selector on the labels of the pods.


## Create as single pod with Service

Create the pod

```
kubectl apply -f nginx-pod.yaml
```

Create service

```
kubectl apply -f nginx-pod-service.yaml
```

Test if the service can be used with its name on port 8080

- go inside the pod
  -  ```kubectl exec -it nginx-pod -- sh```
- curl to the service
  - ```curl http://nginx-pod:8080```  

But now we want to be able to access the service from 
outside the cluster. Let's create an Ingress for this service.

Create an ingress, see nginx-ingress.yaml

```
kubectl apply -f nginx-ingress.yaml
```

## Not reliable

When Pod dies, service dies

Run a curl on the service and kill the pod.

Curl command:

```
watch curl --silent http://demo3.cbs-gilde.local
```

Kill the pod

```
kubectl delete pod nginx-pod
```

This is not an ideal situation. Let's look at [Deployments](../demo4/DEMO4.md).

Cleanup 

```kubectl delete -f .```


[TOC](../README.md)