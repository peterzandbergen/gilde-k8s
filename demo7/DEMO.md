# Keycloak Deployment with Helm

We use [Bitnami helm chart for Keycloak](https://artifacthub.io/packages/helm/bitnami/keycloak)


## Ingress Controller

Change the values to use an Ingress controller instead of a LoadBalancer. 

Adjust **values.yaml**

values.yaml

```
ingress:
    enabled: true
    hostname: demo7.cbs-gilde.local
service:
    type: ClusterIP
```

## Install Keycloak

```
helm install --values values.yaml \
    demo7-keycloak bitnami/keycloak
```

Open the application [Keycloak](http:demo7.cbs-gilde.local)

Cleanup: 
```
helm uninstall demo7-keycloak
```

[TOC](../README.md)