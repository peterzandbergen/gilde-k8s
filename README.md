# Gilde Kubernetes Introduction

Preparation: [Create a cluster](./demo0/DEMO.md)

Demo: [Keycloak Deployment with Helm](demo7/DEMO.md)

Demo: [Make pods accessible](demo3/DEMO.md)

Demo: [Configure Container with ConfigMap](demo5/DEMO.md)

## Links

- [Kubernetes](https://kubernetes.io/)
- [KinD](https://kind.sigs.k8s.io/)
- [Kubectl en Kustomize](https://kubectl.docs.kubernetes.io/)
- [Lens](https://k8slens.dev/)
- Helm Charts
  - [Artifact Hub](https://artifacthub.io)
  - [Kubeapps](https://hub.kubeapps.com/)
- [OperatorHub](https://operatorhub.io/)
- [Bitnami Keycloak Helm Chart](https://artifacthub.io/packages/helm/bitnami/keycloak)
- [Docker Nginx](https://hub.docker.com/_/nginx)
- [Docker busybox](https://hub.docker.com/_/busybox)


[Terminal setup](DEMO-Setup.md)

```
tmux new-session \; split-window -v \; split-window -h
```

Sluit de tmux sessie
```
tmux kill-session
```



