# Demo setup

### Terminal met Tmux



![Tmux Setup](./images/TmuxDemoSetup.png)

- Top Left: ```watch -n 1 kubectl get pods```
- Bottom Left: Commands
- Right: ```watch -n 1 curl http://demox.cbs-gilde.local```


```
tmux new-session \; split-window -v \; split-window -h
```

[TOC](../README.md)
