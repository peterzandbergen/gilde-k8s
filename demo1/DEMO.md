# Demo 1: Containers

Containers zijn makkelijk, snel en snel weer op te ruimen.

## Nginx 

Start een container en open een poort op 18080.

```
docker run -it --rm --name nginx-port -p 18080:80 nginx:alpine
```

Start hetzelfde image maar zet het html volume naar een locale directory.

```
docker run -it --rm --name nginx-volume \
    -p 18080:80 \
    -v $(pwd)/../html:/usr/share/nginx/html:ro \
    nginx:alpine
```

Voor serieuze toepassingen hebben we meer nodig dan Docker.

[TOC](../README.md)